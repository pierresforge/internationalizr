/**
 * Module dependencies.
 */

var mongoose = require('mongoose');


/**
 * Set schema methods.
 */

var plug = module.exports.plug = function plug(fields) {
  return function(schema) {

    /**
     * Add field to schema
     */

    schema.add({
      _i18n: mongoose.Schema.Types.Mixed
    })


    /**
     * Set `translation` for `field` with `locale`.
     *
     * @param {String} field
     * @param {String} locale
     * @param {Mixed} translation
     */

    schema.methods.setTranslation = function setTranslation(field, locale, translation) {

      // Create locale field if not defined.
      if(!this._i18n[locale]) this._i18n[locale] = {};

      // Add translation.
      this._i18n[locale][field] = translation;

    }

  }
}


/**
 * Expose version.
 */

module.exports.version = require('./package.json')['version'];